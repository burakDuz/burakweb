﻿using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using webProje_3.odev_.Models;

namespace webProje_3.odev_.Controllers
{
    [Authorize(Roles ="admin")]
    public class RolYonetimController : Controller
    {
        ApplicationDbContext context = new ApplicationDbContext();

        // GET: RolYonetim
        public ActionResult RolGoster()
        {
          var rolstore=new  RoleStore<IdentityRole>(context);
            var rolManager = new RoleManager<IdentityRole>(rolstore);
          var goster=  rolManager.Roles.ToList();
            return View(goster);
        }
        public ActionResult RolKullaniciEkle()
        {
            return View();
        }
        [HttpPost]
        public ActionResult RolKullaniciEkle(KullaniciRolEkle model)
        {
            var rolstore = new RoleStore<IdentityRole>(context);
            var rolManager = new RoleManager<IdentityRole>(rolstore);

            var userStore = new UserStore<ApplicationUser>(context);
            var userManager = new UserManager<ApplicationUser>(userStore);
          var kullanici=  userManager.FindByName(model.KullaniciAdi);
            if (kullanici!=null)
            {
                if (!userManager.IsInRole(kullanici.Id, model.RolAdi))
                {
                    userManager.AddToRole(kullanici.Id, model.RolAdi);
                }
            }
            

            return RedirectToAction("RolGoster");
        }
    }
}