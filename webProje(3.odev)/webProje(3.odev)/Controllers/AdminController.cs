﻿using System;
using webProje_3.odev_.Models;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.IO;
using System.Web.Routing;

namespace webProje_3.odev_.Controllers
{
    [Authorize(Roles = "admin")]
    public class AdminController : Controller
    {
       
        ArabaincelemeEntities ent = new ArabaincelemeEntities();
        // GET: Admin
        public ActionResult AdminIndex()
        {
            AnaSayfaDTO obj = new AnaSayfaDTO();
            obj.adminDuyuru = ent.adminDuyuru.Where(x => (x.GonderiTarihi <= DateTime.Now)).ToList();//burası order by la alincak
            obj.inceleme = ent.inceleme.Where(x => (x.GonderiTarih <= DateTime.Now)).ToList();
            obj.kayitKn = ent.kayitKn.Where(x => (x.IncelemeID != null)).ToList();
            //foto ve konuda tarihe göre sıralı alıncak
            return View(obj);
            
        }
        public ActionResult IcerikBitir()
        {
            

            return RedirectToAction("incelemeGoster", "Home");
        }
        public ActionResult IcerikEkle()
        {
            //var incBas = ent.inceleme.OrderByDescending(x => x.IncelemeID).Take(1).ToList();
            
            return View();
        }
        [HttpPost]
        public ActionResult IcerikEkle(kayitKn kKonu, HttpPostedFileBase fileSol, HttpPostedFileBase fileSag)
        {
            var siraBas = ent.kayitKn.OrderByDescending(x => x.KonuID).Take(1).ToList();
         
            
            kayitKn k = new kayitKn();
            if (fileSol != null && fileSol.ContentLength > 0)
            {
                MemoryStream memoryStream = fileSol.InputStream as MemoryStream;
                if (memoryStream == null)
                {
                    memoryStream = new MemoryStream();
                    fileSol.InputStream.CopyTo(memoryStream);
                }
               // incEkle.BaslikResim = memoryStream.ToArray();
               k.ResimSol = memoryStream.ToArray();
            }
            
            if (fileSag != null && fileSag.ContentLength > 0)
            {
                MemoryStream memoryStream = fileSag.InputStream as MemoryStream;
                if (memoryStream == null)
                {
                    memoryStream = new MemoryStream();
                    fileSag.InputStream.CopyTo(memoryStream);
                }
                // incEkle.BaslikResim = memoryStream.ToArray();
                k.ResimSag = memoryStream.ToArray();
            }
            k.Yazi = kKonu.Yazi;
           
           
            var incBas = ent.inceleme.OrderByDescending(x => x.IncelemeID).Take(1).ToList();
            if (siraBas[0].IncelemeID != incBas[0].IncelemeID)
            {
                k.Sira = 1;
            }
            else
            {
                k.Sira = siraBas[0].Sira + 1;
            }
            k.IncelemeID = incBas[0].IncelemeID;
            
            ent.kayitKn.Add(k);
            ent.SaveChanges();
            return RedirectToAction("IcerikEkle", "Admin");
        }

        public ActionResult ArabaInceleme()
        {

            var inc = ent.inceleme.ToList();
            return View(inc);
        }
        public ActionResult IncelemeEkle()
        {
            
            return View();
        }
        [HttpPost]
        public ActionResult IncelemeEkle(inceleme inc,HttpPostedFileBase file)
        {
            
            inceleme incEkle = new inceleme();
            if (file != null && file.ContentLength > 0)
            {
                MemoryStream memoryStream = file.InputStream as MemoryStream;
                if (memoryStream == null)
                {
                    memoryStream = new MemoryStream();
                    file.InputStream.CopyTo(memoryStream);
                }
                incEkle.BaslikResim = memoryStream.ToArray();
            }
            incEkle.Baslik = inc.Baslik;
            incEkle.GonderiTarih = DateTime.Now;
            incEkle.YazarAd = User.Identity.Name;
            incEkle.ModelID = inc.ModelID;
            ent.inceleme.Add(incEkle);
            ent.SaveChanges();
            return RedirectToAction("IcerikEkle", "Admin");

        }
        public ActionResult IncelemeSil(int iID)//baglamayi unutma
        {
            ent.inceleme.Remove(ent.inceleme.First(x => x.IncelemeID == iID));
            var kayit = ent.kayitKn.Where(x=>(x.IncelemeID==iID)).ToList();
            if (kayit!=null)
            {
                for (int i = 0; i < kayit.Count; i++)
                {
                    ent.kayitKn.Remove(kayit[i]);
                }
               
            }
           
            ent.SaveChanges();

            return RedirectToAction("ArabaInceleme", "Admin");
        }
        public ActionResult Duyuru()
        {
            var duyuru = ent.adminDuyuru.ToList();

            return View(duyuru);
        }

        public ActionResult DuyuruEkle()
        {


            return View();
        }
        [HttpPost]
        public ActionResult DuyuruEkle(adminDuyuru aD)
        {
            adminDuyuru duyuru = new adminDuyuru();
            duyuru.Duyuru = aD.Duyuru;
            duyuru.GonderiTarihi = DateTime.Now;
            ent.adminDuyuru.Add(duyuru);
            ent.SaveChanges();
            return RedirectToAction("Duyuru","Admin");
        }
        public ActionResult DuyuruSil(int dID)
        {
            ent.adminDuyuru.Remove(ent.adminDuyuru.First(x=>x.DuyuruID==dID));
            ent.SaveChanges();

            return RedirectToAction("Duyuru", "Admin");
        }
    }
}