﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using webProje_3.odev_.Models;

namespace webProje_3.odev_.Controllers
{
    public class HomeController : Controller
    {
        ArabaincelemeEntities ent = new ArabaincelemeEntities();
        public ActionResult IndexEn()
        {
            AnaSayfaDTO obj = new AnaSayfaDTO();
            obj.adminDuyuru = ent.adminDuyuru.Where(x => (x.GonderiTarihi <= DateTime.Now)).ToList();//burası order by la alincak
            obj.inceleme = ent.inceleme.Where(x => (x.GonderiTarih <= DateTime.Now)).ToList();
            obj.kayitKn = ent.kayitKn.Where(x => (x.IncelemeID != null)).ToList();
            //foto ve konuda tarihe göre sıralı alıncak
            return View(obj);
        }
        public ActionResult Index()
        {
            AnaSayfaDTO obj = new AnaSayfaDTO();
            obj.adminDuyuru = ent.adminDuyuru.Where(x => (x.GonderiTarihi<=DateTime.Now)).ToList();//burası order by la alincak
            obj.inceleme = ent.inceleme.Where(x=>(x.GonderiTarih<=DateTime.Now)).ToList();
            obj.kayitKn = ent.kayitKn.Where(x => (x.IncelemeID !=null)).ToList();
            //foto ve konuda tarihe göre sıralı alıncak
            return View(obj);
        }

        public ActionResult Ybasvuru()
        {


            return View();
        }

        public ActionResult Contact()
        {


            return View();
        }
        
        public ActionResult Kyorum()
        {
            AnaSayfaDTO obj = new AnaSayfaDTO();
                obj.model = ent.model.Where(d=>(d.ModelID!=0)).ToList();

            return View(obj);
        }
        public ActionResult incelemeGoster()
        {
            var incBas = ent.inceleme.OrderByDescending(x => x.IncelemeID).Take(1).ToList();
            AnaSayfaDTO obj = new AnaSayfaDTO();
            obj.inceleme = ent.inceleme.OrderByDescending(x => x.IncelemeID).Take(1).ToList();
            int a = incBas[0].IncelemeID;
            obj.kayitKn = ent.kayitKn.Where(x=>(x.IncelemeID==a)).ToList();
            return View(obj);
        }

        public ActionResult incelemeIndex(int Id)
        {
         var tikArttir = ent.inceleme.Where(x=>(x.IncelemeID==Id)).ToList();
            if (tikArttir[0].TiklamaSayisi!=null)
            {
                tikArttir[0].TiklamaSayisi += 1;
            }
            else
            {
                tikArttir[0].TiklamaSayisi = 1;
            }          
            ent.SaveChanges();
            var incBas = ent.inceleme.Where(x=>(x.IncelemeID==Id)).ToList();
            AnaSayfaDTO obj = new AnaSayfaDTO();
            obj.inceleme = ent.inceleme.Where(x => (x.IncelemeID == Id)).ToList();
            int a = incBas[0].IncelemeID;
            obj.kayitKn = ent.kayitKn.Where(x => (x.IncelemeID == a)).ToList();
            return View(obj);
        }
        public ActionResult Ainceleme()
        {
            AnaSayfaDTO obj = new AnaSayfaDTO();
            obj.model = ent.model.Where(d => (d.ModelID != 0)).ToList();
            return View(obj);
        }
        public ActionResult ModelInceleme(int Id)
        {
            AnaSayfaDTO obj = new AnaSayfaDTO();
            obj.inceleme = ent.inceleme.Where(x => (x.ModelID == Id)).ToList();
            if (obj.inceleme!=null)
            {
                return View(obj);
            }
            else
            {
                return View("KAYİTLİ İNCELEME YOK");
            }
           

        }
        public ActionResult YorumEkle()
        {
            return View();
        }
        [HttpPost]
        public ActionResult YorumEkle(kullaniciYorum kYrm)
        {
            kullaniciYorum kYorumKaydet = new kullaniciYorum();
            AnaSayfaDTO obj = new AnaSayfaDTO();
            kYorumKaydet.Kyorum = kYrm.Kyorum;
            kYorumKaydet.KyorumTarihi = DateTime.Now;
            kYorumKaydet.ModelID = kYrm.ModelID;
            kYorumKaydet.KullaniciAdi = kYrm.KullaniciAdi;
            ent.kullaniciYorum.Add(kYorumKaydet);
            ent.SaveChanges();
            return View();
        }
        public ActionResult YorumGoster(int Id)
        {
            AnaSayfaDTO obj = new AnaSayfaDTO();
           // obj.kYorum = ent.kullaniciYorum.Where(x=>(x.ModelID==Id)).ToList();
            return View(obj);
        }

    }

    public class AnaSayfaDTO
    {
        public List<inceleme> inceleme { get; set; }
        public List<adminDuyuru> adminDuyuru { get; set; }
        public List<kayitKn> kayitKn { get; set; }
        public List<model> model { get; set; }
        public List<kullaniciYorum> kYorum { get; set; }
    }
}