﻿using System.Web;
using System.Web.Mvc;

namespace webProje_3.odev_
{
    public class FilterConfig
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new HandleErrorAttribute());
        }
    }
}
