//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace webProje_3.odev_.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class inceleme
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public inceleme()
        {
            this.kayitKn = new HashSet<kayitKn>();
        }
    
        public int IncelemeID { get; set; }
        public string YazarAd { get; set; }
        public Nullable<System.DateTime> GonderiTarih { get; set; }
        public string Baslik { get; set; }
        public Nullable<int> ModelID { get; set; }
        public Nullable<int> TiklamaSayisi { get; set; }
        public byte[] BaslikResim { get; set; }
    
        public virtual model model { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<kayitKn> kayitKn { get; set; }
    }
}
