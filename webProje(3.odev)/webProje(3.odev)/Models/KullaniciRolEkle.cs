﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace webProje_3.odev_.Models
{
    public class KullaniciRolEkle
    {
        public string KullaniciAdi { get; set; }
        public string RolAdi { get; set; }
    }
}