﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(webProje_3.odev_.Startup))]
namespace webProje_3.odev_
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
